### Dmenu-connman

This is a dmenu script - front-end for `connmanctl services`. Written in Python, tested with version 3.9

## Current features

- Connect to services without passphrase
- Connect to services with passphrase
- See security mechanism and signal strength
- Rescan chosen technology

## Dependencies

- dmenu / rofi _(if you want to use rofi, don't forget to change all dmenu occurences to rofi -dmenu in the script itself)_
- pexpect - for managing connmanctl agent and therefore being able to enter a passphrase.

## Usage

- Arguments passed to script will all be passed to dmenu
- Tapping `Escape` key will bring you one step back or exit the program
